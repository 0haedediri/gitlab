## Jackie Chan Collection [1971-2008]-All Jackie Chan Movies

 
  
 
**LINK 🌟 [https://walllowcopo.blogspot.com/?download=2txXI0](https://walllowcopo.blogspot.com/?download=2txXI0)**

 
 
 
 
 
# Jackie Chan Collection [1971-2008]-All Jackie Chan Movies: A Guide for Fans
  
If you are a fan of Jackie Chan, you might be interested in watching all his movies from 1971 to 2008. Jackie Chan is one of the most popular and influential action stars in the world, known for his unique style of martial arts, comedy, and stunts. He has appeared in nearly 150 films, many of which are considered classics of the genre. But how can you find and watch all his movies from 1971 to 2008? Here is a guide to help you out.
  
## What are the Jackie Chan movies from 1971 to 2008?
  
According to Wikipedia[^1^], Jackie Chan began his film career as an extra child actor in the 1962 film Big and Little Wong Tin Bar. Ten years later, he was a stuntman opposite Bruce Lee in 1972's Fist of Fury and 1973's Enter the Dragon. He then had starring roles in several kung fu films, such as 1973's Little Tiger of Canton and 1976's New Fist of Fury. His first major breakthrough was the 1978 kung fu action comedy film Snake in the Eagle's Shadow, which was shot while he was loaned to Seasonal Film Corporation under a two-picture deal. [^1^]
  
He then enjoyed huge success with similar kung fu action comedy films such as 1978's Drunken Master and 1980's The Young Master. Jackie Chan began experimenting with elaborate stunt action sequences in The Young Master [^2^] and especially Dragon Lord (1982). [^3^] 1983's Project A saw the official formation of the Jackie Chan Stunt Team and established Chan's signature style of elaborate, dangerous stunts combined with martial arts and slapstick humor, a style he further developed in a more modern setting with 1984's Wheels on Meals and notably 1985's Police Story, which contained numerous large-scale action scenes  and is considered one of the best action films of all time.
  
Chan continued his style of slapstick martial arts mixed with elaborate stunts in numerous other films, such as the Police Story sequels, the Armour of God series, Project A Part II (1987), Dragons Forever (1988), Twin Dragons (1992), City Hunter (1993), and Drunken Master II (1994), among others. Rumble in the Bronx (1995) made Jackie Chan a mainstream celebrity in North America, leading to a successful Hollywood career with the Rush Hour and Shanghai Noon series. In 2000, Chan produced an animated series Jackie Chan Adventures, which ran until 2005.  In 2010, Jackie Chan appeared in his first dramatic role in an American film, The Karate Kid.  In 2017, the Chinese - Indian co-production Kung Fu Yoga became his highest-grossing film in China.
  
The following is a list of all Jackie Chan movies from 1971 to 2008, based on IMDb[^2^]:
  
- The Blade Spares None (1971)
- The Angry River (1971)
- Fist of Fury (1972)
- Hapkido (1972)
- The Brutal Boxer (1972)
- Game of Death (1972)
- Police Woman (1973)
- The Golden Lotus (1974)
- All in the Family (1975)
- No End of Surprises (1975)
- All Men Are Brothers (1975)
- New Fist of Fury (1976)
- To Kill with Intrigue (1977)
- The Killer Meteors (1977)
- Snake & Crane Arts of Shaolin (1978)
- Snake in the Eagle's Shadow (1978)
- Drunken Master (1978)
- Spiritual Kung Fu (1978)
- Magnificent Bodyguards (1978)
- Half a dde7e20689




